#include <iostream>
#include "roman.h"

void SUT(int i)
{
    RomanTranslator r;
    std::cout << i << " Translates to " << r.ToRoman(i) << std::endl;
}

int main() {
    SUT(42);
    SUT(1);
    SUT(3998);
    SUT(666);
    SUT(100);
}

