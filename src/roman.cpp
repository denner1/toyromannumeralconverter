#include "roman.h"
#include <stdexcept>
std::string RomanTranslator::ToRoman(int i)
{
    if (i > 3999 || i < 0)
        throw std::out_of_range ("Must be between 3,999 and 1"); 
        //numbers beyond these are either impossible or out of scope for this toy
    for (auto x : elements)
        if (i >= x.first)
            return x.second + ToRoman(i - x.first);
    return ""; 
}